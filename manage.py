import os, typer, getpass, asyncio, uvicorn, validators
from database import connect_db
from apps.auth.token import encode_password
from typing import Optional

app = typer.Typer()

@app.command()
def runserver(
    host: Optional[str] = "0.0.0.0",
    port: Optional[int] = 8000
):
    """ Command to run uvicorn server:
        >>> python manage.py runserver --host [HOST: default "0.0.0.0"] --port [PORT: default 8000]
    """

    typer.secho(
        message = f"\nStartup uvicorn server...\n",
        fg = typer.colors.BRIGHT_GREEN)

    uvicorn.run(
        "main:app",
        port = port,
        host = host,
        reload = True)

@app.command()
def makemigrations(
    name: Optional[str] = None
):
    """ Command to create migrations:
        >>> python manage.py makemigrations --name [NAME: default None]
    """

    typer.secho(
        message = f"\nStart create alembic migration file...\n",
        fg = typer.colors.BRIGHT_GREEN)

    if name:
        os.system(f"alembic revision --autogenerate -m { name }")
    else:
        os.system(f"alembic revision --autogenerate")


@app.command()
def migrate():
    """ Command to apply migrations into database.
        >>> python manage.py migrate
    """

    typer.secho(
        message = f"\nStart migrate...\n",
        fg = typer.colors.BRIGHT_GREEN)

    os.system(f"alembic upgrade head")

@app.command()
def createuser():
    """ Command to create user with permissions in DB.
        >>> python manage.py createuser 
    """

    typer.secho(
        message = f"\nCreate user...\n",
        fg = typer.colors.BRIGHT_GREEN)

    while True:

        username = input("Username >>> ")

        if len(username) < 2:
            typer.secho(
                message = f"\nUsername less than 2 characters!\n",
                fg = typer.colors.BRIGHT_RED)

        else:
            break

    while True:

        email = input("Email >>> ")

        if not check_email(email):
            typer.secho(
                message = f"\nEmail is not valid >>> {email}!\n",
                fg = typer.colors.BRIGHT_RED)

        else:
            break

    while True:

        password = getpass.getpass("Password >>> ")

        if len(password) < 8:
            typer.secho(
                message = f"\nPassword less than 8 characters!\n",
                fg = typer.colors.BRIGHT_RED)

            choise = input("Bypass password validation and create user anyway? [Y/N]: ")

            if choise.lower() == "y":
                break

            elif choise.lower() == "n":
                typer.secho(
                    message = "\nReturn...\n",
                    fg = typer.colors.BRIGHT_RED)

        else:
            break

    while True:

        password_again = getpass.getpass("Password (again) >>> ")

        if password != password_again:
            typer.secho(
                message = "\nPasswords don't match!\n",
                fg = typer.colors.BRIGHT_RED)

        else:
            break

    hash_password = asyncio.run(encode_password(password))

    try:

        create_user = f"""
        INSERT INTO public.users 
        (username, email, password, active, admin)
        VALUES
        (\'{username}\', \'{str(email)}\', \'{str(hash_password)}\', true, true);
        """

        connect = connect_db()

        with connect.cursor() as cur:
            cur.execute(create_user)
            connect.commit()

        typer.secho(
            message = f"User \'{username}\' created.",
            fg = typer.colors.GREEN)
    
    except Exception as e:
        connect.rollback()
        typer.secho(
            message = e,
            fg = typer.colors.BRIGHT_RED)

def check_email(
    email: str
):
    """ Mail validation method
    """

    if validators.email(
        value = email):

        return True
    else:
        return False

if __name__ == "__main__":
    app()
