import os, typer, psycopg2


def connect_db():
    """ Database connection method
    """

    try:
        
        connect = psycopg2.connect(
            database=os.getenv('DATABASES_NAME'),
            user=os.getenv('DATABASES_USER'),
            password=os.getenv('DATABASES_PASSWORD'),
            host=os.getenv('DATABASES_HOST'),
            port=os.getenv('DATABASES_PORT'))

        typer.secho(
            f"\nDatabase opened successfully...\n",
            fg=typer.colors.BRIGHT_GREEN)

        return connect

    except Exception as e:
        typer.secho(
            f"\n{e}\n",
            fg=typer.colors.BRIGHT_RED)