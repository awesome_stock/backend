from sqlalchemy import Column, Integer, String, Boolean, ForeignKey, DateTime, Numeric
from sqlalchemy.sql import func
from core.db import Base

class Order(Base):
    """ Order model
    """
    __tablename__ = 'orders'

    id = Column(
        Integer,
        primary_key=True,
        index=True,
        unique=True,
        autoincrement=True)

    totalPrice = Column(
        Numeric(16, 8),
        nullable=False,
        comment="Цена товара")

    createdDate = Column(
        DateTime(timezone = True),
        server_default = func.now(),
        nullable=False,
        comment="Дата создания")

    shipmentMethod = Column(
        String(255),
        nullable=False,
        comment="Способ доставки")

    shipmentCost = Column(
        Numeric(16, 8),
        nullable=False,
        comment="Цена доставки")

    shipmentAddress = Column(
        String(255),
        nullable=False,
        comment="Адрес доставки")

    paymentMethod = Column(
        String(255),
        nullable=False,
        comment="Способ оплаты")

    isDone = Column(
        Boolean,
        server_default = 'f',
        comment="Закрыт")

    status = Column(
        String(255),
        nullable=True,
        comment="Статус")

    comment = Column(
        String(1000),
        nullable=True,
        comment="Комментарий")

order_table = Order.__table__


class OrdersProducts(Base):
    """ OrdersProducts model
    """
    __tablename__ = 'ordersproducts'

    id = Column(
        Integer,
        primary_key=True,
        index=True,
        unique=True,
        autoincrement=True)

    order_id = Column(
        Integer,
        ForeignKey(
            "orders.id", 
            ),
        nullable=False,
        comment="Заказ")

    product_id = Column(
        Integer,
        ForeignKey(
            "product.id", 
            ),
        nullable=False,
        comment="Продукт")

ordersproducts_table = OrdersProducts.__table__

class UserOrders(Base):
    """ UserOrders model
    """
    __tablename__ = 'userorders'

    id = Column(
        Integer,
        primary_key=True,
        index=True,
        unique=True,
        autoincrement=True)

    user_id = Column(
        Integer,
        ForeignKey(
            "users.id", 
            ),
        nullable=False,
        comment="Пользователь")

    order_id = Column(
        Integer,
        ForeignKey(
            "order.id", 
            ),
        nullable=False,
        comment="Заказ")

userorders_table = UserOrders.__table__