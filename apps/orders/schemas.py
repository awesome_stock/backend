from pydantic import BaseModel, condecimal
from typing import List, Optional

class ProdOrder(BaseModel):
    id: int
    name: str
    quantity: int

class RequestOrderSchema(BaseModel):
    """ Order creation/refresh request schema
    """

    user_id: int
    # products:  List[ProdOrder]
    products: str
    totalPrice: condecimal(max_digits=8, decimal_places=2)
    shipmentCost: condecimal(max_digits=8, decimal_places=2)
    shipmentMethod: str
    shipmentAddress: str
    paymentMethod: str
    isDone: Optional[bool] = False
    status: Optional[str] = None
    comment: Optional[str] = None

    class Config:
        schema_extra = {
            "example": {
                "user_id": 1,
                "products": [{"id": 1, "name": "name", "quantity": 1}, {"id": 2, "name": "name", "quantity": 2}],
                "totalPrice": 100.01,
                "shipmentCost": 10.55,
                "shipmentMethod": "YandexGo",
                "shipmentAddress": "Bolshaya Pecherskaya st., 44",
                "paymentMethod": "Cash",
                "comment": "Please call +795824588294"
            }
        }
