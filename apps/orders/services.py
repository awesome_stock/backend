import json
import sqlalchemy as db
from apps.auth.authentication.models import User
from apps.products.models import Product
from core.db import database
from .schemas import RequestOrderSchema
from apps.orders.models import Order, order_table
from apps.auth.send import send_order_email


async def create_order(
    value: RequestOrderSchema
):
    """ Method to create client order
    """
    async with database.transaction():
        user = await database.fetch_one(
            query=db.select([User.id,
                             User.username,
                             User.email,
                             ]).where((User.id == value.user_id)))
    # prods_to_order = []
    # for item in value.products:
    #     async with database.transaction():
    #         product = await database.fetch_all(
    #             query=db.select([Product.id,
    #                             Product.name_ru,
    #                              ]).where((Product.id == item.id)))

    #         for prod in product:
    #             # prods_to_order.append({ "id": prod['id'], "name": prod['name_ru'] })
    #             prods_to_order.append(prod['name_ru'])

    async with database.transaction():
        id = await database.execute(
            order_table.insert().values(
                totalPrice=value.totalPrice,
                shipmentCost=value.shipmentCost,
                shipmentMethod=value.shipmentMethod,
                shipmentAddress=value.shipmentAddress,
                paymentMethod=value.paymentMethod,
                isDone=value.isDone,
                status=value.status,
                comment=value.comment,
            )
        )
    # + json.dumps(prods_to_order)
    prods = "\n" + "Products: " + str(value.products.encode('utf-8')) + "\n"
       
    usnm = "\n" + "User: " + str(user["username"]) + "\n"
    totPrice = "\n" + "Total price: " + str(value.totalPrice) + " RUB " + "\n"
    pMtd = "\n" + "Payment Method: " + str(value.paymentMethod) + "\n"
    shipMtd = "\n" + "Shipment Method: " + str(value.shipmentMethod) + "\n"
    shipAdd = "\n" + "Shipment Address: " + str(value.shipmentAddress) + "\n"
    # comm = "\n" + "Shipment Address: " + str(value.comment) + "\n" 

    text = "Order confirmed! \n" + usnm + prods + totPrice  +\
        pMtd + shipMtd + shipAdd

    await send_order_email(
        email = user["email"],
        message = text)

    return {
        "detail": "Заказ успешно создан."
    }
