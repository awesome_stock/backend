from fastapi import APIRouter, status
from .schemas import RequestOrderSchema
from .services import create_order


router = APIRouter(
    tags=["Client orders"],
    prefix="/order",)


@router.post(
    "/create",
    status_code = status.HTTP_201_CREATED)
async def order_create(
    request: RequestOrderSchema
):
    """ Order creation method
    """

    response = await create_order(
        value = request)

    return response
