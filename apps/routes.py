from fastapi import APIRouter
from apps.auth.router import router as AuthRouter 
from apps.products.router import router as ProductRouter 
from apps.categories.router import router as CategoriesRouter 
from apps.orders.router import router as OrderRouter


routes = APIRouter(
    prefix = "/api",)


routes.include_router(CategoriesRouter)
routes.include_router(ProductRouter)
routes.include_router(AuthRouter)
routes.include_router(OrderRouter)
