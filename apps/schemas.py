from pydantic import BaseModel

class Response(BaseModel):
    """ Standart response scheme
    """

    detail: str = None