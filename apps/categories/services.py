import sqlalchemy as db
from sqlalchemy import func
from fastapi import HTTPException, status
from core.db import database
from .schemas import Category
from .models import Category


async def get_categories(
):
    """ Get all categories method
    """

    async with database.transaction():
        categories = await database.fetch_all(
            query=db.select([Category.id,
                             Category.name_ru]))
    return categories
    # return db.query(models.User).offset(skip).limit(limit).all()

async def category_create(
    value: Category
):
    """ Create_Category method
    """

    async with database.transaction():
        category = Category(name_ru=value.name_ru)
        db.add(category)
        await db.commit()
        db.refresh(category)
        return category

# def get_user(db: Session, user_id: int):
#     return db.query(models.User).filter(models.User.id == user_id).first()


# def get_user_by_email(db: Session, email: str):
#     return db.query(models.User).filter(models.User.email == email).first()





