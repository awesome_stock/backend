from pydantic import BaseModel
from typing import Optional


class Category(BaseModel):
    """ Category
    """

    id: int
    name_ru: str
    name_en: Optional[str] = None
    description_ru: Optional[str] = None
    description_en: Optional[str] = None