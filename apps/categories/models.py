from sqlalchemy import Column, Float, Integer, String, Boolean, ForeignKey, DateTime, Numeric
from sqlalchemy.sql import func
from core.db import Base

class Category(Base):
    """ Category model
    """
    __tablename__ = 'category'

    id = Column(
        Integer,
        primary_key=True,
        index=True,
        unique=True,
        autoincrement=True)

    name_ru = Column(
        String(50),
        index=True,
        unique=True,
        nullable=False,
        comment="Имя подписки на RU.")

    name_en = Column(
        String(50),
        index=True,
        unique=True,
        nullable=True,
        comment="Имя подписки на EN.")

    description_ru = Column(
        String(255),
        nullable=True,
        comment="Подробное описание подписки на RU.")

    description_en = Column(
        String(255),
        nullable=True,
        comment="Подробное описание подписки на EN.")

category_table = Category.__table__