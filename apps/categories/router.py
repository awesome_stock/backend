from fastapi import APIRouter, Header, status
from apps.schemas import Response
from .schemas import Category
from .services import category_create, get_categories

router = APIRouter(
    prefix = "/categories",
    tags = ["Categories"],)


@router.post(
    "/create",
    status_code = status.HTTP_200_OK,
    response_model = Category)
async def create_category(
    request: Category
):
    """ Category endpoint
    """
    
    response = await category_create(
        value = request)
    
    return response

@router.get(
    "/get",
    status_code = status.HTTP_200_OK)
async def categories_get():
    """ Get all categories endpoint
    """

    response = await get_categories()

    return response