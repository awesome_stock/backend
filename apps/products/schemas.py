from pydantic import BaseModel
from typing import Optional


class Product(BaseModel):
    """ Product
    """

    id: int
    name: str
    price: float
    category: int
    description: Optional[str] = None
    rating: float
    width: float
    height: float
    weight: float
    product_code: int
    inventory: int
