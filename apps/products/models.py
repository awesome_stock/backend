from sqlalchemy import Column, Float, Integer, String, Boolean, ForeignKey, DateTime, Numeric
from sqlalchemy.sql import func
from core.db import Base

class Product(Base):
    """ Product model
    """
    __tablename__ = 'product'

    id = Column(
        Integer,
        primary_key=True,
        index=True,
        unique=True,
        autoincrement=True)

    name_ru = Column(
        String(50),
        index=True,
        unique=True,
        nullable=False,
        comment="Имя подписки на RU.")

    name_en = Column(
        String(50),
        index=True,
        unique=True,
        nullable=True,
        comment="Имя подписки на EN.")

    price = Column(
        Float,
        index=True,
        unique=True,
        nullable=False,
        comment="Цена товара")

    rating = Column(
        Float,
        index=True,
        unique=True,
        nullable=False,
        comment="Рейтинг")

    long = Column(
        Float,
        index=True,
        unique=True,
        nullable=False,
        comment="Длина")

    width = Column(
        Float,
        index=True,
        unique=True,
        nullable=False,
        comment="Ширина")

    height = Column(
        Float,
        index=True,
        unique=True,
        nullable=False,
        comment="Высота")

    weight = Column(
        Float,
        index=True,
        unique=True,
        nullable=False,
        comment="Вес")

    product_code = Column(
        Integer,
        index=True,
        unique=True,
        nullable=False,
        comment="Код продукта")

    inventory = Column(
        Integer,
        index=True,
        unique=True,
        nullable=False,
        comment="Количество на складе")

    category_id = Column(
        Integer,
        ForeignKey(
            "category.id", 
            ),
        nullable = False,
        comment = "Идентификатор пользователя, которому принадлежит подписка.")

    description_ru = Column(
        String(255),
        nullable=True,
        comment="Подробное описание подписки на RU.")

    description_en = Column(
        String(255),
        nullable=True,
        comment="Подробное описание подписки на EN.")

product_table = Product.__table__