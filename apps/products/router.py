from fastapi import APIRouter, Header, status
from apps.schemas import Response
from .schemas import Product
from .services import product_create, get_products

router = APIRouter(
    prefix="/products",
    tags=["Products"],)


@router.post(
    "/create",
    status_code=status.HTTP_200_OK,
    response_model=Product)
async def create_product(
    request: Product
):
    """ Product endpoint
    """

    response = await product_create(
        value=request)

    return response


@router.get(
    "/get",
    status_code=status.HTTP_200_OK)
async def products_get():
    """ Get all products endpoint
    """
    response = await get_products()
    return response
