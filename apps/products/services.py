import sqlalchemy as db
from sqlalchemy import func
from fastapi import HTTPException, status
from core.db import database
from .schemas import Product
from .models import Product


async def get_products():
    """ Get_products method
    """
    async with database.transaction():
        products = await database.fetch_all(
            query=db.select([Product.id,
                             Product.name_ru,
                             Product.name_en,
                             Product.price,
                             Product.category_id,
                             Product.rating,
                             Product.long,
                             Product.width,
                             Product.height,
                             Product.weight,
                             Product.product_code,
                             Product.inventory,
                             Product.description_ru,
                             Product.description_en]))
    return products

async def product_create(
    value: Product
):
    """ Create_products method
    """

    prod = Product.create(value)

    # async with database.transaction():
    #     products = await database.
    return products