from sqlalchemy import Column, Integer, String, DateTime, Boolean
from sqlalchemy.sql import func
from core.db import Base


class User(Base):
    """ User model
    """

    __tablename__ = 'users'

    id = Column(
        Integer,
        primary_key = True,
        index = True,
        unique = True,
        autoincrement = True)

    username = Column(
        String(30),
        index = True,
        unique = True, 
        nullable = False,
        comment = "Имя пользователя")

    email = Column(
        String(100),
        index = True,
        unique = True, 
        nullable = False,
        comment = "Почта")

    password = Column(
        String(1000),
        nullable = False,
        comment = "Пароль")

    active = Column(
        Boolean,
        server_default = 'f',
        comment = "Активен")

    admin = Column(
        Boolean,
        server_default = 'f',
        comment = "Права администратора")

    blocked = Column(
        Boolean,
        server_default = 'f',
        comment = "Заблокирован")
    
    created = Column(
        DateTime(timezone = True),
        server_default = func.now(),
        comment = "Дата создания")
    
    updated = Column(
        DateTime(timezone = True),
        onupdate = func.now(),
        comment = "Дата последнего обновления")

users_table = User.__table__