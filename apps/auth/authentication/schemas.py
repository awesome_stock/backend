from pydantic import BaseModel
from typing import Optional


class UserLogin(BaseModel):
    """ Scheme of request at user login
    """

    login: str
    password: str

    class Config:
        schema_extra = {
            "example": {
                "login": "CCleaner",
                "password": "HelloWorld"
            }
        }


class RefreshToken(BaseModel):
    """ Scheme of request at update token
    """

    access_token: str
    refresh_token: str

    class Config:
        schema_extra = {
            "example": {
                "access_token": "JWT",
                "refresh_token": "JWT"
            }
        }


class UserInfo(BaseModel):
    """ Authorized user information
    """

    id: int
    username: str
    is_admin: bool
    is_active: bool
    is_blocked: bool
    date_created: str
    date_updated: Optional[str] = None
