import sqlalchemy as db
from sqlalchemy import func
from fastapi import HTTPException, status
from core.db import database
from apps.auth.token import verify_password, create_auth_token, token_repair, token_verify, decode_token
from .schemas import UserLogin, RefreshToken
from .models import User


async def create_auth(
    value: UserLogin
):
    """ Authorization method
    """  

    async with database.transaction():
        user = await database.fetch_one(
            query = db.select([User.id,
                               User.username,
                               User.password,
                               User.active,
                               User.blocked]).where((User.email == value.login) | 
                                                    (User.username == value.login)))
        

    check_user_status(
        user = user)

    verify = await verify_password(
        password = value.password,
        hash = user["password"])

    if value.login and not verify:
        raise HTTPException(
            status_code = status.HTTP_403_FORBIDDEN,
            detail = "Wrong login or password.")

    new_token = await create_auth_token(
        user = user["username"],
        token = user["id"])

    return new_token 


async def refresh_auth(
    value: RefreshToken
):
    """ Authorization refresh method
    """

    repair_token = await token_repair(
        token = value.refresh_token,
        salt = value.access_token)

    decode_repair_token = await decode_token(
        token = repair_token)

    verify_repair_token = await token_verify(
        decode = decode_repair_token,
        destination = "_refreshToken",
        connection = value.access_token)

    async with database.transaction():
        user = await database.fetch_one(
            query = db.select([User.id,
                               User.username,
                               User.active,
                               User.blocked]).where(User.id == verify_repair_token["token"]))

    check_user_status(
        user = user)

    new_token = await create_auth_token(
            user = user["username"],
            token = user["id"])

    return new_token


async def logout_auth(
    value: RefreshToken
):
    """ Logout method
    """

    return {
        "detail": "Logout success."
    }


async def owner_auth(
    token: str
):
    """ Method for getting a user by token
    """

    token = await decode_token(
        token = token)

    verify_token = await token_verify(
        decode = token,
        destination = "_accessToken")   

    async with database.transaction():
        user = await database.fetch_one(
            query = db.select([User.id,
                               User.username,
                               User.admin,
                               User.active,
                               User.blocked,
                               func.to_char(User.created, 'dd.mm.yyyy hh24:mi:ss').label("created"),
                               func.to_char(User.updated, 'dd.mm.yyyy hh24:mi:ss').label("updated")]).where(User.id == verify_token["token"]))

    check_user_status(
        user = user)

    return {
        "id": user.id,
        "username": user.username,
        "is_admin": user.admin,
        "is_active": user.active,
        "is_blocked": user.blocked,
        "date_created": user.created,
        "date_updated": user.updated
    }


def check_user_status(
    user
):
    """ User status check method
    """

    if not user:
        raise HTTPException(
            status_code = status.HTTP_404_NOT_FOUND,
            detail = "User account not found.")

    if not user["active"]:
        raise HTTPException(
            status_code = status.HTTP_403_FORBIDDEN,
            detail = "User account not activated.")

    if user["blocked"]:
        raise HTTPException(
            status_code = status.HTTP_403_FORBIDDEN,
            detail = "User account is blocked.")
