from fastapi import APIRouter, Header, status
from apps.schemas import Response
from .schemas import UserLogin, RefreshToken, UserInfo
from .services import create_auth, refresh_auth, logout_auth, owner_auth


router = APIRouter(
    prefix = "/token",
    tags = ["Auth | token create, refresh, owner, logout."],)


@router.post(
    "/create",
    status_code = status.HTTP_200_OK,
    response_model = RefreshToken)
async def create_token(
    request: UserLogin
):
    """ Authorization endpoint
    """
    
    response = await create_auth(
        value = request)
    
    return response


@router.post(
    "/refresh",
    status_code = status.HTTP_200_OK,
    response_model = RefreshToken)
async def refresh_token(
    request: RefreshToken
):
    """ Refresh authorization endpoint
    """

    response = await refresh_auth(
        value = request)

    return response


@router.post(
    "/logout",
    status_code = status.HTTP_200_OK,
    response_model = Response)
async def logout(
    request: RefreshToken
):
    """ Logout endpoint
    """

    response = await logout_auth(
        value = request)

    return response


@router.get(
    "/owner",
    status_code = status.HTTP_200_OK,
    response_model = UserInfo)
async def owner_token(
    Authorization: str = Header(...)
):
    """ Get current user info endpoint
    """
    
    response = await owner_auth(
        token = Authorization)

    return response
