from pydantic import BaseModel, EmailStr


class UserAccount(BaseModel):
    """ User account creation scheme
    """

    email: EmailStr
    username: str
    password: str

    class Config:
        schema_extra = {
            "example": {
                "email": "g.unknown@yandex.ru",
                "username": "CCleaner",
                "password": "HelloWorld"
            }
        }


class UserAccess(BaseModel):
    """ Access to account from mail scheme
    """

    token: str

    class Config:
        schema_extra = {
            "example": {
                "token": "JWT"
            }
        }


class ResetPassword(BaseModel):
    """ Account password recovery request scheme
    """

    email: EmailStr = None
    username: str = None

    class Config:
        schema_extra = {
            "example": {
                "email": "behavioruser@gmail.com"
            }
        }


class ResetPasswordAccess(UserAccess):
    """ Account password recovery response scheme
    """

    password: str

    class Config:
        schema_extra = {
            "example": {
                "token": "JWT",
                "password": "HelloWorld!"
            }
        }
