import sqlalchemy as db
from .schemas import UserAccount
from core.db import database
from fastapi import HTTPException, status
from apps.auth.authentication.models import User, users_table
from apps.auth.token import encode_password, encode_token, decode_token, token_verify
from apps.auth.send import send_signup_email
from apps.auth.authentication.services import check_user_status


async def create_user_account(
    value: UserAccount
):
    """ User account creation method
    """

    async with database.transaction():
        user = await database.fetch_one(
            query = db.select([User.email,
                               User.username]).where((User.email == value.email) |
                                                     (User.username == value.username)))

    if user and value.email == user["email"]:
        raise HTTPException(
            status_code = status.HTTP_400_BAD_REQUEST,
            detail = f"Email \'{value.email}\' already exists.")

    if user and value.username == user["username"]:
        raise HTTPException(
            status_code = status.HTTP_400_BAD_REQUEST,
            detail = f"User \'{value.username}\' already exists.")

    async with database.transaction():
        id = await database.execute(
            users_table.insert().values(
                username = value.username,
                email = value.email,
                password = await encode_password(
                    password = value.password)))

    if not id:
        raise HTTPException(
            status_code = status.HTTP_400_BAD_REQUEST,
            detail = f"User \'{value.username}\' already exists.")

    # token = await encode_token(
    #     data = {
    #         "user": value.username,
    #         "token": id,
    #         "destination": "_signUpToken" 
    #     })

    text = f"Вы зарегистировались на сайте \'Бытовая техника\':\nВаш логин: {value.username}\nВаш пароль: {value.password}"

    await send_signup_email(
        email = value.email,
        message = text.encode('utf-8'))
    
    return {
        "detail": "Successful mail sending."
    }