from fastapi import APIRouter, status
from apps.schemas import Response
from .schemas import UserAccount, UserAccess, ResetPassword, ResetPasswordAccess
from .services import create_user_account


router = APIRouter(
    prefix = "/email",
    tags = ["Email | email sign-up."],)


@router.post(
    "/sign-up",
    status_code = status.HTTP_201_CREATED,
    response_model = Response)
async def create_account(
    request: UserAccount
):
    """ User account registration endpoint
    """

    response = await create_user_account(
        value = request)

    return response
