from fastapi import HTTPException, status
from jose import jwt
from typing import Optional
from datetime import timedelta, datetime
from passlib.context import CryptContext
from typing import Optional
from core.config import ALGORITHM_PASS, SECRET_KEY, ALGORITHM, ACCESS_TOKEN_EXPIRE_MINUTES, ACCESS_TOKEN_LIFETIME


pwd_hash = CryptContext(
    schemes=[ALGORITHM_PASS],
    deprecated="auto")

async def encode_password(
    password: str
):
    """ Method for getting password hash
    """

    return pwd_hash.hash(password)

async def verify_password(
    password: str,
    hash: str
):
    """ Password verification method
    """

    verify = pwd_hash.verify(
        secret = password,
        hash = hash)

    return verify

async def encode_token(
    data: dict,
    expires_delta: Optional[timedelta] = None
):
    """ Token encoding method
    """

    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    
    else:
        expire = datetime.utcnow() + timedelta(minutes = ACCESS_TOKEN_EXPIRE_MINUTES)

    payload = data.copy()

    payload.update({
        "exp": expire
    })

    token = jwt.encode(
        claims = payload,
        key = SECRET_KEY,
        algorithm = ALGORITHM)

    return token


async def decode_token(
    token: str
):
    """ Token decoding method
    """

    try:

        payload = jwt.decode(
            token = token,
            key = SECRET_KEY,
            algorithms = [ALGORITHM])

        if not payload:
            raise HTTPException(
                status_code = status.HTTP_401_UNAUTHORIZED,
                detail = "Invalid payload for authorization token.")

        return payload

    except jwt.ExpiredSignatureError:
        raise HTTPException(
            status_code = status.HTTP_401_UNAUTHORIZED,
            detail = "Authorization token is expired.")

    except jwt.JWTClaimsError as err:
        raise HTTPException(
            status_code = status.HTTP_401_UNAUTHORIZED,
            detail = str(err))

    except jwt.JWTError:
        raise HTTPException(
            status_code = status.HTTP_401_UNAUTHORIZED,
            detail = "Invalid signature.")   
    
    except Exception as e:
        raise HTTPException(
            status_code = status.HTTP_401_UNAUTHORIZED,
            detail = f"Authorization header cannot be parsed.\n{e}")


async def token_salt(
    token: str,
    salt: str
):
    """ Method for adding salt to token
    """

    admixture = f"{token}{salt[:10]}"
    rotate = admixture[::-1]

    return rotate


async def token_repair(
    token: str,
    salt: str
):
    """ Token recovery method
    """

    original_salt = salt[:10]
    rotate = token[::-1]

    find_salt = rotate[-10:]
    delete_salt = rotate[:-10]

    if original_salt != find_salt:
        raise HTTPException(
            status_code = status.HTTP_400_BAD_REQUEST,
            detail = "Invalid signature.")

    return delete_salt


async def token_verify(
    decode: dict,
    destination: str,
    connection: Optional[str] = None
):
    """ Token verification method
    """

    if "destination" not in decode:
        raise HTTPException(
            status_code = status.HTTP_401_UNAUTHORIZED,
            detail = "No rights.")

    if decode["destination"] != destination:
        raise HTTPException(
            status_code = status.HTTP_401_UNAUTHORIZED,
            detail = f"Rights are not valid.")

    if "token" not in decode:
        raise HTTPException(
            status_code = status.HTTP_401_UNAUTHORIZED,
            detail = "Token ID not detected.")

    if "user" not in decode:
        raise HTTPException(
            status_code = status.HTTP_401_UNAUTHORIZED,
            detail = "Token holder not detected.")

    if connection:
        if "connection" not in decode or decode["connection"] != connection:
            raise HTTPException(
                status_code = status.HTTP_401_UNAUTHORIZED,
                detail = f"Token holder not detected.")

    return decode    


async def create_auth_token(
    user: str,
    token: int
): 
    """ Token creation method
    """

    # Create access token (active 5 minutes, configure in .env)
    access_token = await encode_token(
        data = {
            "user": user,
            "token": token,
            "destination": "_accessToken"
        }, 
        expires_delta = timedelta(minutes = ACCESS_TOKEN_LIFETIME))

    # Create refresh token (active 1 day)
    refresh_token = await encode_token(
        data = {
            "user": user,
            "token": token,
            "connection": access_token,
            "destination": "_refreshToken"
        })

    salted_token = await token_salt(
        token = refresh_token,
        salt = access_token)

    return {
        "access_token": access_token,
        "refresh_token": salted_token
    }
