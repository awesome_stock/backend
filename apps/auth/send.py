import emails
import smtplib
from pathlib import Path
from emails.template import JinjaTemplate
from fastapi import HTTPException, status
from core.config import PROJECT_NAME, PROJECT_EMAIL, CORS_ORIGIN_CLIENT, SMTP


async def send_email(
    email: str,
    subject: str,
    html: str,
    environment: dict
):
    """ Email sending method
    """

    server = smtplib.SMTP_SSL(
        host='smtp.mail.ru',
        port=465)

    server.login(
        user='secommerce@bk.ru',
        password='abHU8a749uA5xLMMWurS')

    server.sendmail(
        from_addr='secommerce@bk.ru',
        to_addrs=email,
        msg=environment['link'])

    server.quit()


async def send_order_email(
    email: str,
    message: str = None
):
    """ Method for sending an email with order info
    """
    try:
        subject = f"Order confirmed"

        with open(Path("apps/auth/email/template") / "access.html") as template:
            html = template.read()

        await send_email(
            email=email,
            subject=subject,
            html=html,
            environment={
                "team_name": PROJECT_NAME,
                "email": email,
                "link": 'Order confirmed: ' + message,
                "token": ''
            })

    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"Send message error.\n{e}")


async def send_signup_email(
    email: str,
    message: str = None
):
    """ Method for sending an email with user registration
    """
    try:
        subject = f"{PROJECT_NAME}/Registration"

        with open(Path("apps/auth/email/template") / "access.html") as template:
            html = template.read()

        await send_email(
            email=email,
            subject=subject,
            html=html,
            environment={
                "team_name": PROJECT_NAME,
                "email": email,
                "link": message,
                "token": ''
            })

    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"Send message error.\n{e}")
