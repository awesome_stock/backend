from fastapi import APIRouter
from .authentication.router import router as authentication_router
from .email.router import router as email_router

router = APIRouter(
    prefix="/auth",)

router.include_router(authentication_router)
router.include_router(email_router)