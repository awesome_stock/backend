from core.db import Base
from apps.auth.authentication.models import User
from apps.orders.models import Order, OrdersProducts
from apps.products.models import Product
from apps.categories.models import Category