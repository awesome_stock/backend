import os
from dotenv import load_dotenv

# Load .env file
load_dotenv()

# Password hash
ALGORITHM_PASS = os.getenv('ALGORITHM_PASS')

# Database connection
SQLALCHEMY_DATABASE_URL = f"postgresql://{os.getenv('DATABASES_USER')}:{os.getenv('DATABASES_PASSWORD')}@{os.getenv('DATABASES_HOST')}:{os.getenv('DATABASES_PORT')}/{os.getenv('DATABASES_NAME')}"

# Secret key for token authentication
SECRET_KEY = os.getenv('SECRET_KEY')
# Algorithm for token create
ALGORITHM = os.getenv('ALGORITHM')
# Default token lifetime
ACCESS_TOKEN_EXPIRE_MINUTES = int(os.getenv('ACCESS_TOKEN_EXPIRE_MINUTES'))
# Set token lifetime
ACCESS_TOKEN_LIFETIME = int(os.getenv('ACCESS_TOKEN_LIFETIME'))

# Project name for email registration
PROJECT_NAME = os.getenv('PROJECT_NAME')
PROJECT_EMAIL = os.getenv('PROJECT_EMAIL')

# SMTP configuration
SMTP = {
    'TLS': True,
    'PORT': os.getenv('SMTP_PORT'),
    'HOST': os.getenv('SMTP_HOST'),
    'USER': os.getenv('SMTP_USER'),
    'PASSWORD': os.getenv('SMTP_PASSWORD'),
}

# Link to client
CORS_ORIGIN_CLIENT = os.getenv('CORS_WHITELIST_CLIENT')
