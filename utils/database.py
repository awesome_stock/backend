from sqlalchemy.sql.schema import Table as table
from core.db import database


async def insert(
    table: table, 
    value
):
    """ Method for inserting values into a table
    """
    
    query = table.insert().values(**value.dict())
    
    pk = await database.execute(
        query=query)

    return {
        "id": pk,
        **value.dict()
    }